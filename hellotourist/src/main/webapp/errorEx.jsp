<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP404 Page</title>
        <style>
.center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;
}
        </style>
    </head>
    
    <body style="background-color: #fee;">

        <h1 class="center">Unexpected error, please try again later</h1>
        <img class="center" src="./sad.jpeg"> 
        <h1 class="center"><a href="./test">Return to Homepage</a> </h1>
    </body>
</html>
