package hu.ps.ht.util;

import hu.ps.ht.dto.BookingDTO;
import hu.ps.ht.enumerated.OutcomeMessage;
import hu.ps.ht.service.BookingService;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class BookingUtil {

    @Inject
    BookingService bookingService;

    public String operationHelper(char select, BookingDTO bookingDTO) {
        OutcomeMessage result;
        String strResponse = "";
        switch (select) {
            case 'A':

                result = bookingService.cancelBooking(bookingDTO);
                if (null != result) {
                    if (result == OutcomeMessage.success) {
                        strResponse = "Canceled successfully!";
                    } else {
                        strResponse = "Cancel Failed!";
                    }
                }

                ;
                break;
            case 'B':

                result = bookingService.confirmBooking(bookingDTO);
                if (null != result) {
                    if (result == OutcomeMessage.success) {
                        strResponse = "Confirmed successfully!";
                    } else {
                        strResponse = "Confirm Failed!";
                    }
                }

                ;
                break;
            case 'C':

                result = bookingService.completeBooking(bookingDTO);
                if (null != result) {
                    if (result == OutcomeMessage.success) {
                        strResponse = "Completed successfully!";
                    } else {
                        strResponse = "Completed Failed!";
                    }
                }

        }
        return strResponse;

    }

}
