package hu.ps.ht.util;

public class DatabaseConnectivityException extends Exception {

   public DatabaseConnectivityException(String msg) {
        super(msg);
    }
}
