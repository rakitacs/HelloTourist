/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.ps.ht.servlet;

import hu.ps.ht.dto.BookingDTO;
import hu.ps.ht.dto.GuideDTO;
import hu.ps.ht.enumerated.BookingStatus;
import hu.ps.ht.service.BookingService;
import hu.ps.ht.service.GuideService;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.List;
import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "CreateBookingServlet", urlPatterns = {"/createbooking"})
public class CreateBookingServlet extends HttpServlet {

    @Inject
    BookingService bookingService;

    @Inject
    GuideService gs;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            String str = "";
//******************  COLLISIONCHECK***********************          
            BookingDTO bookingDTO = new BookingDTO(null, 9999L, "description majd az event/bol lesz betoltve", LocalDate.now().minusDays(3L), "Borisz", "Hikari", BookingStatus.COMPLETED, new StringBuffer());

            try {
                bookingService.checkForDateCollision(bookingDTO);
                bookingService.createNewBooking(bookingDTO);
                BookingDTO testDTO = bookingService.findBookingById(1l);
                str += "<br> eza 1. booking>>> <br>" + testDTO.toString();
            } catch (EJBException | IllegalArgumentException e) {
                str += "<br>********************";
                str += "<br>********************";
                str += "<br>********************";
                str += "<br>" + e.toString();
                str += "<br>********************";
                str += "<br>********************";
            }

//******************  COLLISIONCHECK***********************          
            BookingDTO bookingDTO2 = new BookingDTO(null, 1234L, "ez a masodik booking", LocalDate.now().plusDays(5), "Borisz", "Hikari", BookingStatus.COMPLETED, new StringBuffer());
            try {
                bookingService.checkForDateCollision(bookingDTO2);
                bookingService.createNewBooking(bookingDTO2);
                BookingDTO testDTO2 = bookingService.findBookingById(2l);
                str += "<br> eza 2. booking>>> <br>" + testDTO2.toString();
            } catch (EJBException | IllegalArgumentException e) {
                str += "<br>********************";
                str += "<br>********************";
                str += "<br>********************";
                str += "<br>" + e.toString();
                str += "<br>********************";
                str += "<br>********************";
            }

            BookingDTO bookingDTO3 = new BookingDTO(null, 65559L, "ez Maxim bookingja", LocalDate.now(), "Maxim", "Hikari", BookingStatus.COMPLETED, new StringBuffer());
            try {
                bookingService.checkForDateCollision(bookingDTO3);
                bookingService.createNewBooking(bookingDTO3);
                BookingDTO testDTO3 = bookingService.findBookingById(3L);
                str += "<br> eza 3. booking MAXIM>>> <br>" + testDTO3.toString();
            } catch (EJBException | IllegalArgumentException e) {
                str += "<br>********************";
                str += "<br>********************";
                str += "<br>********************";
                str += "<br>" + e.toString();
                str += "<br>********************";
                str += "<br>********************";
            }

            BookingDTO bookingDTO4 = new BookingDTO(null, 100000L, "ez a 4 booking", LocalDate.now().plusDays(4), "Borisz", "Hikari", BookingStatus.CONFIRMED, new StringBuffer(""));
            try {
                bookingService.checkForDateCollision(bookingDTO4);
                bookingService.createNewBooking(bookingDTO4);
                BookingDTO testDTO4 = bookingService.findBookingById(4l);
                str += "<br> eza 4. booking>>> <br>" + testDTO4.toString();
            } catch (EJBException | IllegalArgumentException e) {
                str += "<br>********************";
                str += "<br>********************";
                str += "<br>********************";
                str += "<br>" + e.toString();
                str += "<br>********************";
                str += "<br>********************";
            }

            BookingDTO bookingDTO5 = new BookingDTO(null, 100000L, "ez a 5 booking", LocalDate.now().minusDays(15), "Borisz", "Hikari", BookingStatus.EVALUATED, new StringBuffer("test sringbuff5booking"));
            try {
                bookingService.checkForDateCollision(bookingDTO5);
                bookingService.createNewBooking(bookingDTO5);
                BookingDTO testDTO5 = bookingService.findBookingById(5l);
                str += "<br> eza 5. booking>>> <br>" + testDTO5.toString();
            } catch (EJBException | IllegalArgumentException e) {
                str += "<br>********************";
                str += "<br>********************";
                str += "<br>********************";
                str += "<br>" + e.toString();
                str += "<br>********************";
                str += "<br>********************";
            }

            GuideDTO gdto = gs.getGuideByUsername("Borisz");
            List<BookingDTO> bdtos = bookingService.getAllBookingsQueryGuideName(gdto.getUserName());
            if (bdtos != null) {
                for (BookingDTO bdto : bdtos) {
                    System.out.println("\nEz Borisz egyik foglalasa***************");
                    str += "<br>Ez Borisz egyik foglalasa***************";
                    System.out.println("\n" + bdto.toString());
                    str += "<br>" + bdto.toString();
                }
            }

            GuideDTO gdto2 = gs.getGuideByUsername("Maxim");
            List<BookingDTO> bdtos2 = bookingService.getAllBookingsQueryGuideName(gdto2.getUserName());
            if (bdtos2 != null) {
                for (BookingDTO bdto : bdtos2) {
                    System.out.println("\nEz MAxim egyik foglalasa***************");
                    str += "<br>Ez MAxim egyik foglalasa***************";
                    System.out.println("\n" + bdto.toString());
                    str += "<br>" + bdto.toString();
                }
            }

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CreateBookingServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println(str);
            out.println("<h1>Servlet CreateBookingServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
