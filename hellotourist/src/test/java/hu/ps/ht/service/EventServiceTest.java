package hu.ps.ht.service;

import hu.ps.ht.dao.EventDAO;
import hu.ps.ht.dao.GuideDAO;
import hu.ps.ht.dto.EventDTO;
import hu.ps.ht.entity.CategoryEntity;
import hu.ps.ht.entity.EventEntity;
import hu.ps.ht.entity.GuideEntity;
import hu.ps.ht.entity.ReservationEntity;
import hu.ps.ht.enumerated.Region;
import hu.ps.ht.enumerated.WeeklyPattern;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.NoResultException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;

/*
  public EventDTO getEventDtoById(Long id) {
        try {
            EventEntity eventEntity = eventDAO.find(EventEntity.class, id);
            EventDTO eventDTO = EventMapper.mapEventEntityToDto(eventEntity);
            return eventDTO;
        } catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(EventService.class.getName()).log(Level.SEVERE, null, ex);
        } return null;
    }

    public List<EventDTO> getEventDtoListByRegion(String input) {
        if (regionService.checkIfRegionExists(input)) {
            List<EventEntity> eventEntityList = eventDAO.getEventListByRegion(Region.valueOf(input));
            List<EventDTO> eventDtoList = EventMapper.mapEventEntityListToDtoList(eventEntityList);
            System.out.println("EVENTDTO SIZE " + eventDtoList.size());
            return eventDtoList;
        } else {
            return null;
        }

    }

    public List<EventDTO> getEventDtoListByGuide(String guidename) {
        List<EventEntity> eventEntitys = eventDAO.getEventEntityListByGuide(guidename);
        return EventMapper.mapEventEntityListToDtoList(eventEntitys);
    }

    public void createNewEventFromDTO(EventDTO eventDTO) {

        EventEntity eventEntity = EventEntity.builder()
                .category(eventDTO.getCategory())
                .cost(eventDTO.getCost())
                .dayOfWeek(eventDTO.getDayOfWeek())
                .description(eventDTO.getDescription())
                .guide(guideDAO.getGuideByUserName(eventDTO.getGuideUserName()))
                .maxParticipants(eventDTO.getMaxParticipants())
                .region(eventDTO.getRegion())
                .build();
        eventDAO.createEntity(eventEntity);

    }*/
@ExtendWith(MockitoExtension.class)
public class EventServiceTest {

    private static final Long KEY = 1L;
    @Mock
    private EventDAO eventDAO;
    @Mock
    private GuideDAO guideDAO;
    @Mock
    private EventEntity ee;
    @Mock
    private GuideEntity ge;
    @Mock
    private CategoryEntity ce;
    @Mock
    private EventDTO ed;
    @Mock
    private RegionService regionService;

    @InjectMocks
    private EventService underTest;

    @Test
    public void testGetEventDtoListByRegion() throws Exception {

        EventDTO eventEntity = new EventDTO();
        EventDTO eventEntity2 = new EventDTO();
//        EventEntity eventEntity = new EventEntity();
//        EventEntity eventEntity2 = new EventEntity();

        List<EventDTO> mockList = new ArrayList<>();

        eventEntity2.setId(1L);
        eventEntity2.setDescription("nekem a");
        eventEntity2.setMaxParticipants(10L);
        eventEntity2.setCost(10000L);
        eventEntity2.setGuideUserName("Borisz");
//        eventEntity2.setGuide(ge);
        eventEntity2.setDayOfWeek(WeeklyPattern.ALWAYS_VACANT);
        eventEntity2.setCategory(new CategoryEntity("Asztro"));
        eventEntity2.setRegion(Region.AGGTELEK);

        eventEntity.setId(2L);
        eventEntity.setDescription("nekem a");
        eventEntity.setMaxParticipants(10L);
        eventEntity.setCost(10000L);
        eventEntity.setGuideUserName("Borisz");
//        eventEntity.setGuide(ge);
        eventEntity.setDayOfWeek(WeeklyPattern.ALWAYS_VACANT);
        eventEntity.setCategory(new CategoryEntity("Asztro"));
        eventEntity.setRegion(Region.BALATON);

        mockList.add(eventEntity);
        mockList.add(eventEntity2);

        EventService myService = mock(EventService.class, Mockito.RETURNS_DEEP_STUBS);
        when(myService.getEventDtoListByRegion("BALATON")).thenReturn(mockList);

        assertEquals(2, myService.getEventDtoListByRegion("BALATON").size());
        assertEquals(0, myService.getEventDtoListByRegion("VELENCEI").size());
        System.out.println("getEventDtoListByRegion");

    }

    @Test
    public void testCreateNewEventFromDTO() throws Exception {
//        
        EventDTO eventDTO1 = new EventDTO();
        eventDTO1.setId(KEY);
        eventDTO1.setDescription("nekem a");
        eventDTO1.setMaxParticipants(10L);
        eventDTO1.setCost(10000L);
        eventDTO1.setGuideUserName("Borisz");
        eventDTO1.setDayOfWeek(WeeklyPattern.ALWAYS_VACANT);
        eventDTO1.setCategory(new CategoryEntity("Asztro"));
        eventDTO1.setRegion(Region.BALATON);

        EventEntity eventEntity = new EventEntity();
        GuideEntity guide = new GuideEntity();
        CategoryEntity category = new CategoryEntity("Asztro");
        List<EventEntity> mockList = new ArrayList<>();
        mockList.add(new EventEntity());
        Set<ReservationEntity> mockSet = new HashSet<>();
        mockSet.add(new ReservationEntity(LocalDate.now(), "Borisz", guide));

        guide.setAverageScore(Double.MAX_VALUE);
        guide.setEmail("a@b.c");
        guide.setEventList(mockList);
        guide.setId(KEY);
        guide.setImageLink("./v");
        guide.setNumOfReviews(1);
        guide.setOperatesInRegion(Region.BALATON);
        guide.setPhone("12345678");
        guide.setReservedDates(null);
        guide.setScoreSum(100L);
        guide.setUserName("Borisz");

        eventEntity.setId(KEY);
        eventEntity.setDescription("nekem a");
        eventEntity.setMaxParticipants(10L);
        eventEntity.setCost(10000L);
        eventEntity.setGuide(guide);
        eventEntity.setDayOfWeek(WeeklyPattern.ALWAYS_VACANT);
        eventEntity.setCategory(category);
        eventEntity.setRegion(Region.BALATON);
        underTest.createNewEventFromDTO(ed); //create EventEntity
        verify(eventDAO).createEntity(ArgumentMatchers.any());
    }
}
