/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.ps.ht.service;

import hu.ps.ht.dto.GuideDTO;
import hu.ps.ht.enumerated.BookingStatus;
import hu.ps.ht.enumerated.Region;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * public void evaluateGuide(GuideDTO inputGuideDTO, Integer evaluatedScore) {
 *
 * System.out.println("************ evaluateGuide CALLED! *"); GuideEntity
 * guideEntityRetrieved = null; GuideEntity guideEntityRetrieved2 = null;
 * GuideDTO guideDTO = null;
 *
 * guideEntityRetrieved =
 * guideDAO.getGuideByUserName(inputGuideDTO.getUserName());
 * System.out.println("****** CHECK guideEntityRetrieved"); try { guideDTO =
 * GuideMapper.mapGuideEntityToDTO(guideEntityRetrieved); } catch
 * (IllegalAccessException ex) {
 * Logger.getLogger(BookingService.class.getName()).log(Level.SEVERE, null, ex);
 * } catch (InvocationTargetException ex) {
 * Logger.getLogger(BookingService.class.getName()).log(Level.SEVERE, null, ex);
 * }
 *
 * guideDTO.setScoreSum(guideDTO.getScoreSum() + evaluatedScore);
 * guideDTO.setNumOfReviews(guideDTO.getNumOfReviews() + 1);
 * guideDTO.setAverageScore(Math.floor(( guideDTO.getScoreSum() / (double)
 * guideDTO.getNumOfReviews() )*1e2)/1e2 );
 *
 *
 * try { guideEntityRetrieved2 = guideDAO.find(GuideEntity.class,
 * inputGuideDTO.getId()); } catch (Exception ex) { System.out.println("ex: " +
 * ex.getLocalizedMessage()); }
 * guideEntityRetrieved2.setScoreSum(guideDTO.getScoreSum());
 * guideEntityRetrieved2.setNumOfReviews(guideDTO.getNumOfReviews());
 * guideEntityRetrieved2.setAverageScore(guideDTO.getAverageScore());
 *
 * try { guideDAO.updateEntity(guideEntityRetrieved); } catch (Exception ex) {
 * System.out.println("Store evaluation failed!"); }
 *
 * }
 *
 * public List<GuideDTO> findAll() {
 *
 * List<GuideEntity> guideEntityList = guideDAO.findAll(GuideEntity.class);
 * return GuideMapper.mapGuideEntityListToDtoList(guideEntityList); }
 *
 * public List<GuideDTO> getGuideDtoListByRegion(Region region) {
 *
 * List<GuideEntity> entityList = guideDAO.getGuideListByRegion(region);
 *
 * List<GuideDTO> dtoList = GuideMapper.mapGuideEntityListToDtoList(entityList);
 * return dtoList;
 *
 * }
 *
 * public GuideDTO getGuideByUsername(String username) {
 *
 * GuideDTO guideDTO = new GuideDTO();// null GuideEntity entity =
 * guideDAO.getGuideByUserName(username); try { guideDTO =
 * GuideMapper.mapGuideEntityToDTO(entity); } catch (IllegalAccessException ex)
 * { Logger.getLogger(GuideService.class.getName()).log(Level.SEVERE, null, ex);
 * } catch (InvocationTargetException ex) {
 * Logger.getLogger(GuideService.class.getName()).log(Level.SEVERE, null, ex); }
 *
 * System.out.println("guideDTO test service" + guideDTO); return guideDTO;
 *
 * }
 *
 * public boolean guideCanConfirm(boolean isGuide, BookingStatus status,
 * LocalDate tourDate) {
 *
 * return (isGuide && status == BookingStatus.AWAITING_CONFIRMATION &&
 * !tourDate.isBefore(LocalDate.now()));
 *
 * }
 *
 * public boolean guideCanComplete(boolean isGuide, BookingStatus bookingStatus,
 * LocalDate tourDate) { return (isGuide && !(tourDate).isAfter(LocalDate.now())
 * && bookingStatus == BookingStatus.CONFIRMED); }
 *
 */
@ExtendWith(MockitoExtension.class)
public class GuideServiceTest {

    public GuideServiceTest() {
    }

    @Mock
    GuideService underTest;

    @Test
    public void testEvaluateGuide() throws Exception {
        System.out.println("evaluateGuide");
        GuideDTO inputGuideDTO = null;
        Integer evaluatedScore = null;

    }

//    @Test
//    public void testFindAll() throws Exception {
//        System.out.println("findAll");
//        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
//        GuideService instance = (GuideService)container.getContext().lookup("java:global/classes/GuideService");
//        List<GuideDTO> expResult = null;
//        List<GuideDTO> result = instance.findAll();
//        assertEquals(expResult, result);
//        container.close();
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    @Test
    public void testGetGuideDtoListByRegion() throws Exception {

        GuideDTO guideDTO = new GuideDTO();
        GuideDTO guideDTO2 = new GuideDTO();

        List<GuideDTO> mockList = new ArrayList<>();

        mockList.add(guideDTO);
        mockList.add(guideDTO2);

        GuideService myService = mock(GuideService.class, Mockito.RETURNS_DEEP_STUBS);
        when(myService.getGuideDtoListByRegion(Region.BUDAPEST)).thenReturn(mockList);

        assertEquals(2, myService.getGuideDtoListByRegion(Region.BUDAPEST).size());
        assertEquals(0, myService.getGuideDtoListByRegion(Region.AGGTELEK).size());

    }

    @Test
    public void testGetGuideByUsername() throws Exception {

        GuideDTO guide = new GuideDTO();
        GuideDTO guide2 = new GuideDTO();
        List<GuideDTO> mockList = new ArrayList<>();
        guide.setUserName("Borisz");
        guide2.setUserName("Antal");
        mockList.add(guide);
        mockList.add(guide2);

        GuideService myService = mock(GuideService.class, Mockito.RETURNS_DEEP_STUBS);

        when(myService.getGuideDtoListByRegion(Region.BUDAPEST)).thenReturn(mockList);
        assertEquals(2, myService.getGuideDtoListByRegion(Region.BUDAPEST).size());
        assertEquals(0, myService.getGuideDtoListByRegion(Region.AGGTELEK).size());

    }

    @Test
    public void testGuideCanConfirm() throws Exception {

        GuideDTO guide = new GuideDTO();
        GuideDTO guide2 = new GuideDTO();
        List<GuideDTO> mockList = new ArrayList<>();
        guide.setUserName("Borisz");
        guide2.setUserName("Antal");
        mockList.add(guide);
        mockList.add(guide2);

        when(underTest.guideCanConfirm(true, BookingStatus.AWAITING_CONFIRMATION, LocalDate.now().plusDays(10))).thenReturn(true);
        when(underTest.guideCanConfirm(false, BookingStatus.AWAITING_CONFIRMATION, LocalDate.now().plusDays(10))).thenReturn(false);
        when(underTest.guideCanConfirm(true, BookingStatus.COMPLETED, LocalDate.now().plusDays(10))).thenReturn(false);
        when(underTest.guideCanConfirm(true, BookingStatus.EVALUATED, LocalDate.now().plusDays(10))).thenReturn(false);

        assertEquals(true, underTest.guideCanConfirm(true, BookingStatus.AWAITING_CONFIRMATION, LocalDate.now().plusDays(10)));
        assertEquals(false, underTest.guideCanConfirm(false, BookingStatus.AWAITING_CONFIRMATION, LocalDate.now().plusDays(10)));
        assertEquals(false, underTest.guideCanConfirm(true, BookingStatus.COMPLETED, LocalDate.now().plusDays(10)));
        assertEquals(false, underTest.guideCanConfirm(true, BookingStatus.EVALUATED, LocalDate.now().plusDays(10)));
    }

    @Test
    public void testGuideCanComplete() throws Exception {
        when(underTest.guideCanComplete(true, BookingStatus.AWAITING_CONFIRMATION, LocalDate.now().plusDays(10))).thenReturn(true);
        when(underTest.guideCanComplete(false, BookingStatus.AWAITING_CONFIRMATION, LocalDate.now().plusDays(10))).thenReturn(false);
        when(underTest.guideCanComplete(true, BookingStatus.COMPLETED, LocalDate.now().plusDays(10))).thenReturn(false);
        when(underTest.guideCanComplete(true, BookingStatus.EVALUATED, LocalDate.now().plusDays(10))).thenReturn(false);

        assertEquals(true, underTest.guideCanComplete(true, BookingStatus.AWAITING_CONFIRMATION, LocalDate.now().plusDays(10)));
        assertEquals(false, underTest.guideCanComplete(false, BookingStatus.AWAITING_CONFIRMATION, LocalDate.now().plusDays(10)));
        assertEquals(false, underTest.guideCanComplete(true, BookingStatus.COMPLETED, LocalDate.now().plusDays(10)));
        assertEquals(false, underTest.guideCanComplete(true, BookingStatus.EVALUATED, LocalDate.now().plusDays(10)));
    }
}
